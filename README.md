**PSA:** This repository has been moved to my own infrastructure. For details
on how to migrate your installation, please refer to this reddit post: 
https://www.reddit.com/r/SurfaceLinux/comments/cdhw3e/changes_to_the_linuxsurface_fedora_repository/

# Fedora linux-surface Repository
This is a repository containing packages for the linux-surface kernel, as well as
various utilities that are neccessary or recommended for using linux on Microsoft
Surface devices.

All packages in this repository are built from https://github.com/StollD/fedora-linux-surface

## Installing the repository
You can add the repository to dnf using the following commands
```bash
$ sudo dnf config-manager --add-repo=https://gitlab.com/StollD/fedora-linux-surface-repo/raw/f30/linux-surface.repo
$ sudo dnf config-manager --enablerepo linux-surface
```

## Installing linux-surface
You have to download jakedays repository and run the included `setup.sh` script, which installs
required configuration files and binary firmware blobs for WiFi and touchscreen. When it asks 
you to install the patched libwacom and kernel, select no, since that would start downloading
`.deb` packages that are useless on Fedora.
```bash
$ git clone https://github.com/jakeday/linux-surface
$ cd linux-surface
$ bash setup.sh
```
You only need to do this once and it will continue to work just fine. If jakeday at some point updates the
script with files or commands relevant to your device, you should rerun it.

## Installing the kernel
First you need to install the secureboot certificate that the kernels are signed 
with. You can sign them yourselves of course and not install the certificate. Then,
install the patched kernel and the patched libwacom (required for proper rotation
of the Surface Pen).
```bash
$ sudo dnf install linux-surface-secureboot
$ sudo dnf install kernel-surface libwacom-surface
```

If you own a Gen 5 Surface device (SB2 / S2017 or newer), you should install the
autorotation service for the buttons:
```bash
$ sudo dnf install linux-surfacegen5-button-autoremap
$ sudo systemctl enable surfacebook2-button-autoremap.service
```

If you own a Surface Book you should install the surface DTX daemon as well 
(unsure about other models)
```bash
$ sudo dnf install linux-surface-dtx-daemon
$ sudo systemctl enable surface-dtx-daemon.service
$ systemctl --user enable surface-dtx-userd.service
```

Then, reboot the device to boot into the new kernel. At the UEFI screen, you will 
be greeted with a big blue screen, that says `MOK Management`. Confirm that you 
want to enroll my secureboot certificate. When it asks for a password enter `000`,
then reboot. You should be able to start the surface kernel just fine now.

## Surface wakes up from hibernation immideately
If your surface doesn't hibernate correctly, but restarts instead, this can happen due to 
misleading signals from the USB 3.0 controller and other wakeup sources. You can disable them,
by creating a file `/etc/systemd/system/disable-usb-wakeup.service` with the following content
```ini
[Unit]
Description=Disable USB wakeup triggers in /proc/acpi/wakeup

[Service]
Type=oneshot
ExecStart=/bin/sh -c "echo EHC1 > /proc/acpi/wakeup; echo EHC2 > /proc/acpi/wakeup; echo XHC > /proc/acpi/wakeup"
ExecStop=/bin/sh -c "echo EHC1 > /proc/acpi/wakeup; echo EHC2 > /proc/acpi/wakeup; echo XHC > /proc/acpi/wakeup"
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```
and enabling / starting the service
```bash
$ sudo systemctl enable disable-usb-wakeup.service
$ sudo systemctl start disable-usb-wakeup.service
```
Your surface should now hibernate correctly.
